package com.example.d_chat;

import com.example.d_chat.Notifications.MyResponse;
import com.example.d_chat.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAa-raUYE:APA91bHvdZBYsg84coeHznEfW_FItB7rI3nDudOKoDAjv5LOWjgecjf437clGjTZSV0ZJWCRzTps0ExPMSjsSXI5LcJ8O871R7lZw7UazLVKcm35qrFTSDYdqDONFHJ_cgrr25fzd3Lv"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}