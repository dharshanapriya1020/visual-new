package com.example.d_chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.d_chat.Adapter.ContactList_Adapter;
import com.example.d_chat.DataModels.Chat;
import com.example.d_chat.DataModels.Chatlist;
import com.example.d_chat.DataModels.User;
import com.example.d_chat.Notifications.Token;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Chat_Main extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ContactList_Adapter contactList_adapter;
    private List<User> mUsers;
    private List<String> stringList = new ArrayList<>();
    FirebaseUser firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
    DatabaseReference reference;
    ImageView imageView;
     TextView last_message;

    FirebaseUser fuser;

    private List<Chatlist> usersList;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_main);
        // Chat_Main.this.setTitle("Hello");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.logo);

        last_message = findViewById(R.id.id_last_message);


        recyclerView = findViewById(R.id.id_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        mUsers = new ArrayList<>();
       // readUsers();


        usersList = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                                    .child("follower");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                String  str = (String) snapshot.getValue();
                    stringList.add(str);
                }

               //  chatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        DatabaseReference  reference1 = FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                .child("following");
        reference1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    String  str = (String) snapshot.getValue();
                    stringList.add(str);
                }

                chatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        updateToken(FirebaseInstanceId.getInstance().getToken());
    }

    // fun to update token

    private void updateToken(String token){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1 = new Token(token);
        reference.child(fuser.getUid()).setValue(token1);
    }

    private void chatList() {
        mUsers = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);
                    for (int i=0;i<stringList.size();i++){
                        if(!user.getId().equals(firebaseUser.getUid())) {
                              if (user.getId().equals(stringList.get(i))){
                            mUsers.add(user);
                        }
                        }
                    }
                }
                contactList_adapter = new ContactList_Adapter(getApplicationContext(), mUsers, true);
                recyclerView.setAdapter(contactList_adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void readUsers() {

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);

                    if (!user.getId().equals(firebaseUser.getUid())) {

                        for (Chatlist chatlist : usersList) {
                            if (user.getId().equals(chatlist.getId())) {
                                mUsers.add(user);
                            }
                        }
//                        DatabaseReference reference1 = FirebaseDatabase.getInstance().getReference("Follow")
//                                .child(firebaseUser.getUid()).child("follower");
//
//                           reference1.addValueEventListener(new ValueEventListener() {
//                               @Override
//                               public void onDataChange(@NonNull DataSnapshot snapshot) {
//                                   for(DataSnapshot snapshot1:dataSnapshot.getChildren()){
//                                       stringList.add(snapshot1.toString());
//                                   }
//                                   for(Chatlist chatlist:usersList){
//                                       if(user.getId().equals(chatlist.getId())){
//                                           mUsers.add(user);
//                                       }
//                                   }
////                                   for (int i = 0; i < stringList.size(); i++) {
////                                       if(user.getId().toString().equals(stringList.get(i))){
////                                           mUsers.add(user);
////                                       }
////                                   }
//
//
//                               }
//
//                               @Override
//                               public void onCancelled(@NonNull DatabaseError error) {
//
//                               }
//                           });
                    }
                        contactList_adapter = new ContactList_Adapter(getApplicationContext(), mUsers, true);
                        recyclerView.setAdapter(contactList_adapter);
//                        mUsers.add(user);

//                    contactList_adapter = new ContactList_Adapter(getApplicationContext(), mUsers, true);
//                    recyclerView.setAdapter(contactList_adapter);
                }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {

        getMenuInflater().inflate(R.menu.chat_main_options, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item ) {

        switch (item.getItemId()){
            case R.id.profile:
                Intent intent = new Intent(Chat_Main.this,Profile_Activity.class);
                startActivity(intent);
                break;
            case R.id.search:
                Toast.makeText(this, "Refresh Clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void status(String status){
        reference = FirebaseDatabase.getInstance().getReference("Users").child(fuser.getUid());

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);

        reference.updateChildren(hashMap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        status("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        status("offline");
    }
}