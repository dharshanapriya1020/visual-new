package com.example.d_chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.d_chat.DataModels.User;
import com.example.d_chat.Fragraments.MainFragment;
import com.example.d_chat.Fragraments.ProfileFragment;
import com.example.d_chat.Fragraments.SearchFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ImageView menuIcon;
    ConstraintLayout contentView;
    ChipNavigationBar chipNavigationBar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Context context;

    // for Main profile
    CircleImageView image_profile;
    TextView username;

    DatabaseReference reference;
    FirebaseUser fuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.CreateTheme1);
        setContentView(R.layout.activity_home);

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.main_logo);


        image_profile = findViewById(R.id.id_main_profile);
        image_profile.setCropToPadding(true);

        menuIcon = findViewById(R.id.idmenu_icon);
        contentView = findViewById(R.id.content);
        //Menu Hooks
        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.navigator);
        navigationDrawer();
        //bottom chip navigation
        chipNavigationBar = findViewById(R.id.idbottom_navigation);
        chipNavigationBar.setItemSelected(R.id.idhome, true);
        getSupportFragmentManager().beginTransaction().replace(R.id.idfragment_container, new MainFragment()).commit();
        bottomMenu();


        // permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE}, 10
            );




        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(fuser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                assert user != null;
//                username.setText(user.getUsername());
                if (user.getImageURL().equals("default")){
                    image_profile.setImageResource(R.mipmap.ic_launcher);
                } else {
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(image_profile);
                   // Picasso.get().load(user.getImageURL()).into(imageView);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        // click image go to profile

        image_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.idfragment_container, fragment).commit();
            }
        });


    }

    private void bottomMenu() {


        chipNavigationBar.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int i) {
                Fragment fragment = null;
                switch (i) {
                    case R.id.idhome:
                        fragment = new MainFragment();
                        break;
                    case R.id.idsearch:
                        fragment = new SearchFragment();
                        break;
                    case R.id.idprofile:
                        fragment = new ProfileFragment();
                        break;
                }
                assert fragment != null;
                getSupportFragmentManager().beginTransaction().replace(R.id.idfragment_container, fragment).commit();
            }
        });
    }

    //right navigation Drawer
    public void navigationDrawer() {

        //Navigation Drawer
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.idchat);

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END))
                    drawerLayout.closeDrawer(GravityCompat.END);
                else
                    drawerLayout.openDrawer(GravityCompat.END);
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.id_requests:
                return true;
            case R.id.idchat:
                    Intent chatActivity = new Intent(HomeActivity.this, Chat_Main.class);
                    startActivity(chatActivity);
                    return true;
            case R.id.idfriends_post:
                Intent friends_post_activity = new Intent(HomeActivity.this, Friends_Post_Activity.class);
                startActivity(friends_post_activity);
                return true;
            case R.id.idvideos:
                return true;
            case R.id.idphotos:
                return true;
            case R.id.idgames:
                return true;

            case R.id.idlog_out:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(HomeActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                return true;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerVisible(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            if (chipNavigationBar.getSelectedItemId() == R.id.idhome) {
                super.onBackPressed();
                finish();
            } else {
                chipNavigationBar.setItemSelected(R.id.idhome, true);
            }
            //super.onBackPressed();

        }
    }

}