package com.example.d_chat.Adapter;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.d_chat.DataModels.User;
import com.example.d_chat.Fragraments.ProfileFragment;
import com.example.d_chat.HomeActivity;
import com.example.d_chat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Search_user_Adapter extends RecyclerView.Adapter<Search_user_Adapter.ImageViewHolder> {
    private Context mContext;
    private List<User> mUsers;
    private boolean isFragment;

    private FirebaseUser firebaseUser;

    public Search_user_Adapter(Context context, List<User> users, boolean isFragment) {
        mContext = context;
        mUsers = users;
        this.isFragment = isFragment;
    }

    @NonNull
    @Override
    public Search_user_Adapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.searcha_user_items, parent, false);
        return new Search_user_Adapter.ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Search_user_Adapter.ImageViewHolder holder, final int position) {

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        final User user = mUsers.get(position);
        process1(holder, user.getId());
        process2(holder, user.getId());

        holder.btn_request.setVisibility(View.VISIBLE);
        isFollowing(user.getId(), holder.btn_request);

        holder.username.setText(user.getUsername());
        // holder.fullname.setText(user.getFullname());
        Glide.with(mContext).load(user.getImageURL()).into(holder.image_profile);

        if (user.getId().equals(firebaseUser.getUid())) {
            holder.btn_request.setVisibility(View.GONE);
        }


//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isFragment) {
//                    SharedPreferences.Editor editor = mContext.getSharedPreferences("PREFS", MODE_PRIVATE).edit();
//                    editor.putString("profileid", user.getId());
//                    editor.apply();
//
//                    ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction().replace(R.id.idfragment_container,
//                            new ProfileFragment()).commit();
//                } else {
//                    Intent intent = new Intent(mContext, HomeActivity.class);
//                    intent.putExtra("publisherid", user.getId());
//                    mContext.startActivity(intent);
//                }
//            }
//        });

        holder.btn_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.btn_request.getText().toString().equals("Request")) {
                    FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                            .child("Waiting").child(user.getId()).setValue(user.getId());
                    FirebaseDatabase.getInstance().getReference().child("Follow").child(user.getId())
                            .child("Requesting").child(firebaseUser.getUid()).setValue(firebaseUser.getUid());

                    addNotification(user.getId());
                } else {
                    FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                            .child("Waiting").child(user.getId()).removeValue();
                    FirebaseDatabase.getInstance().getReference().child("Follow").child(user.getId())
                            .child("Requesting").child(firebaseUser.getUid()).removeValue();
                }

            }

        });


       holder.btn_accept.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

                   FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                           .child("follower").child(user.getId()).setValue(user.getId());
                   FirebaseDatabase.getInstance().getReference().child("Follow").child(user.getId())
                           .child("following").child(firebaseUser.getUid()).setValue(firebaseUser.getUid());
                   FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                           .child("Requesting").child(user.getId()).removeValue();
                   FirebaseDatabase.getInstance().getReference().child("Follow").child(user.getId())
                           .child("Waiting").child(firebaseUser.getUid()).removeValue();
                   holder.btn_follower.setVisibility(View.VISIBLE);
                   holder.btn_reject.setVisibility(View.INVISIBLE);
                   holder.btn_accept.setVisibility(View.INVISIBLE);
                   holder.btn_request.setVisibility(View.INVISIBLE);
                   //addNotification(user.getId());
           }
       });


       holder.btn_reject.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                       .child("Requesting").child(user.getId()).removeValue();
               FirebaseDatabase.getInstance().getReference().child("Follow").child(user.getId())
                       .child("Waiting").child(firebaseUser.getUid()).removeValue();
               holder.btn_accept.setVisibility(View.INVISIBLE);
               holder.btn_reject.setVisibility(View.INVISIBLE);
               holder.btn_following.setVisibility(View.INVISIBLE);
               holder.btn_follower.setVisibility(View.INVISIBLE);
               holder.btn_request.setVisibility(View.VISIBLE);
           }
       });


       holder.btn_follower.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                       .child("follower").child(user.getId()).removeValue();
               FirebaseDatabase.getInstance().getReference().child("Follow").child(user.getId())
                       .child("following").child(firebaseUser.getUid()).removeValue();
               holder.btn_accept.setVisibility(View.INVISIBLE);
               holder.btn_reject.setVisibility(View.INVISIBLE);
               holder.btn_following.setVisibility(View.INVISIBLE);
               holder.btn_follower.setVisibility(View.INVISIBLE);
               holder.btn_request.setVisibility(View.VISIBLE);
           }
       });

        holder.btn_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid())
                        .child("following").child(user.getId()).removeValue();
                FirebaseDatabase.getInstance().getReference().child("Follow").child(user.getId())
                        .child("follower").child(firebaseUser.getUid()).removeValue();
                holder.btn_accept.setVisibility(View.INVISIBLE);
                holder.btn_reject.setVisibility(View.INVISIBLE);
                holder.btn_following.setVisibility(View.INVISIBLE);
                holder.btn_follower.setVisibility(View.INVISIBLE);
                holder.btn_request.setVisibility(View.VISIBLE);

            }
        });
        isFollowing(user.getId(), holder.btn_request);


    }

    private void process2(ImageViewHolder holder, String userid) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("Follow").child(firebaseUser.getUid()).child("following");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(userid).exists()) {
                    holder.btn_accept.setVisibility(View.INVISIBLE);
                    holder.btn_reject.setVisibility(View.INVISIBLE);
                    holder.btn_request.setVisibility(View.INVISIBLE);
                    holder.btn_following.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        DatabaseReference reference1 = FirebaseDatabase.getInstance().getReference()
                .child("Follow").child(firebaseUser.getUid()).child("follower");
        reference1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(userid).exists()) {
                    holder.btn_accept.setVisibility(View.INVISIBLE);
                    holder.btn_reject.setVisibility(View.INVISIBLE);
                    holder.btn_request.setVisibility(View.INVISIBLE);
                    holder.btn_following.setVisibility(View.INVISIBLE);
                    holder.btn_follower.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    private void process1(ImageViewHolder holder, String userid) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("Follow").child(firebaseUser.getUid()).child("Requesting");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(userid).exists()) {
                    holder.btn_accept.setVisibility(View.VISIBLE);
                    holder.btn_reject.setVisibility(View.VISIBLE);
                    holder.btn_request.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void addNotification(String userid){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifications").child(userid);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("userid", firebaseUser.getUid());
        hashMap.put("text", "started following you");
        hashMap.put("postid", "");
        hashMap.put("ispost", false);

        reference.push().setValue(hashMap);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        public TextView username;
        public TextView fullname;
        public CircleImageView image_profile;
        public Button btn_request,btn_accept,btn_reject,btn_follower,btn_following;

        public ImageViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.username);
            fullname = itemView.findViewById(R.id.fullname);
            image_profile = itemView.findViewById(R.id.image_profile_search_users);
            btn_request = itemView.findViewById(R.id.btn_request);
            btn_accept = itemView.findViewById(R.id.btn_accept);
            btn_reject = itemView.findViewById(R.id.btn_reject);
            btn_follower = itemView.findViewById(R.id.btn_follower);
            btn_following = itemView.findViewById(R.id.btn_following);
        }
    }

    private void isFollowing(final String userid, final Button button){

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("Follow").child(firebaseUser.getUid()).child("Waiting");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(userid).exists()){
                    button.setText("Waiting");
                }else{
                    button.setText("Request");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
