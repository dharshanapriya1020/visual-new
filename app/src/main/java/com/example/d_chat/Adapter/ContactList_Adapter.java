package com.example.d_chat.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.d_chat.Chat_Main;
import com.example.d_chat.Chatting_Room;
import com.example.d_chat.DataModels.Chat;
import com.example.d_chat.DataModels.User;
import com.example.d_chat.MainActivity;
import com.example.d_chat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class ContactList_Adapter extends RecyclerView.Adapter<ContactList_Adapter.ViewHolder> {

    private Context context;
    private List<User>dataModels;
    private boolean ischat;
    String theLastMessage;
    DatabaseReference reference;

    public ContactList_Adapter(Context context, List<User> dataModels, boolean ischat) {
        this.context = context;
        this.dataModels = dataModels;
        this.ischat = ischat;
    }

    @NonNull
    @Override
    public ContactList_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact_list, parent, false);
        return new ContactList_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactList_Adapter.ViewHolder holder, int position) {

        final User user = dataModels.get(position);
        holder.cardView.setBackgroundResource(R.drawable.round_corner);
        holder.name.setText(user.getUsername());
        if (user.getImageURL().equals("default")){
            holder.profile.setImageResource(R.mipmap.ic_launcher);
        } else {
            Glide.with(context).load(user.getImageURL()).into(holder.profile);
        }
        holder.active.animate().rotation(180).start();
        holder.inactive.animate().rotation(180).start();

        if (ischat){
            lastMessage(user.getId(), holder.last_message);
        } else {
            holder.last_message.setVisibility(View.GONE);
        }

        if(ischat){
            if(user.getHigh_light().equals("yes")) {
                highlight_msg(holder, user.getId());
            }

        }


        if (ischat){
            if (user.getStatus().equals("online")){
                holder.active.setVisibility(View.VISIBLE);
                holder.inactive.setVisibility(View.INVISIBLE);
            } else {
                holder.active.setVisibility(View.INVISIBLE);
                holder.inactive.setVisibility(View.VISIBLE);
            }
        } else {
            holder.active.setVisibility(View.INVISIBLE);
            holder.inactive.setVisibility(View.VISIBLE);
        }

        holder.cardView.setOnClickListener(view -> {
            Intent intent = new Intent(context, Chatting_Room.class);
            intent.putExtra("userid", user.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
    }

    private void highlight_msg(ViewHolder holder, String user) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        reference = FirebaseDatabase.getInstance().getReference("Chats");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Chat chat = snapshot.getValue(Chat.class);
                        assert chat != null;
                        if (!chat.isIsseen()) {
                            if (firebaseUser != null) {
                                if (chat.getReceiver().equals(firebaseUser.getUid()) && chat.getSender().equals(user))
                                    holder.last_message.setTextColor(Color.parseColor("#FDACBA"));
                            } else {
                                holder.last_message.setTextColor(Color.parseColor("#605B5B"));
                            }
                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    @Override
    public int getItemCount() {
        return dataModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,last_message;
        public ImageView profile,active,inactive;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.id_cardview);
            active = itemView.findViewById(R.id.id_active);
            inactive = itemView.findViewById(R.id.id_inactive);
            name = itemView.findViewById(R.id.id_name);
            last_message = itemView.findViewById(R.id.id_last_message);
            profile = itemView.findViewById(R.id.id_profile_image);


        }
    }

    private void lastMessage(final String userid, final TextView last_msg){
        theLastMessage = "default";
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chats");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);
                    if (firebaseUser != null && chat != null) {
                        if (chat.getReceiver().equals(firebaseUser.getUid()) && chat.getSender().equals(userid) ||
                                chat.getReceiver().equals(userid) && chat.getSender().equals(firebaseUser.getUid())) {
                            theLastMessage = chat.getMessage();
                        }
                    }
                }

                switch (theLastMessage){
                    case  "default":
                        last_msg.setText("No Message");
                        break;

                    default:
                        last_msg.setText(theLastMessage);
                        break;
                }

                theLastMessage = "default";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
