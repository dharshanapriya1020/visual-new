package com.example.d_chat.DataModels;

public class User {
    private String id;
    private String username;
    private String imageURL;
    private String status;
    private String search;
    private String high_light;

    public User() {
    }

    public User(String id, String username, String imageURL, String status, String search,String high_light) {
        this.id = id;
        this.username = username;
        this.imageURL = imageURL;
        this.status = status;
        this.search = search;
        this.high_light=high_light;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getHigh_light() {
        return high_light;
    }

    public void setHigh_light(String high_light) {
        this.high_light = high_light;
    }
}
