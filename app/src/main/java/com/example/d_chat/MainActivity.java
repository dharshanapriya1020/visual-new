package com.example.d_chat;

import static com.example.d_chat.R.color.dark_blue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.d_chat.DataModels.LoginDataModels;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private EditText edtxt_username,edtxt_password,edtxt_emailId;
    private Chip chip_login,chip_register;
    private TextView create_accounts,txt_registration,txt_login;
    FirebaseAuth auth;
    DatabaseReference reference;
    FirebaseUser firebaseUser;
    private String username,password,emailId;

    @Override
    protected void onStart() {
        super.onStart();

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        //check if user is null
        if (firebaseUser != null){
            Intent intent = new Intent(MainActivity.this, Chat_Main.class);
            startActivity(intent);
            finish();
        }
    }
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setTheme(R.style.CreateTheme);

        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        // setTheme(dark_blue);
        txt_login=findViewById(R.id.id_txt_login);
        txt_registration=findViewById(R.id.id_txt_registration);
        edtxt_username=findViewById(R.id.id_edtxt_username);
        edtxt_emailId=findViewById(R.id.id_edtxt_emailId);
        edtxt_password=findViewById(R.id.id_edtxt_password);
        chip_register=findViewById(R.id.id_chip_Register);
        chip_login=findViewById(R.id.id_chip_login);
        create_accounts=findViewById(R.id.id_txt_create_new_account);
         auth = FirebaseAuth.getInstance();
        create_accounts.setOnClickListener(view -> {
            txt_registration.setVisibility(View.VISIBLE);
            txt_login.setVisibility(View.INVISIBLE);
            edtxt_username.setVisibility(View.VISIBLE);
            chip_register.setVisibility(View.VISIBLE);
            create_accounts.setVisibility(View.INVISIBLE);
       });

        chip_register.setOnClickListener(view -> {
            username =  edtxt_username.getText().toString().trim();
            emailId=edtxt_emailId.getText().toString().trim();
            password = edtxt_password.getText().toString().trim();

            if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(emailId)){
                Toast.makeText(MainActivity.this,"All fields are required",Toast.LENGTH_LONG).show();
            }else{
                register(username,emailId,password);
            }
        });

        chip_login.setOnClickListener(view -> {
            emailId =  edtxt_emailId.getText().toString().trim();
            password = edtxt_password.getText().toString().trim();
            if (TextUtils.isEmpty(emailId) || TextUtils.isEmpty(password)){
                Toast.makeText(MainActivity.this, "All fileds are required", Toast.LENGTH_SHORT).show();
            }else{
                login(emailId,password);
            }
        });

    }

    private void login(String emailId, String password) {
        auth.signInWithEmailAndPassword(emailId, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Intent intent = new Intent(MainActivity.this, Chat_Main.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Authentication failed!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void register(String username,String emailId, String password) {

        auth.createUserWithEmailAndPassword(emailId,password)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        FirebaseUser firebaseUser = auth.getCurrentUser();
                        String userid = firebaseUser.getUid();
                        reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("id", userid);
                        hashMap.put("username", username);
                        hashMap.put("imageURL", "default");
                        hashMap.put("status", "offline");
                        hashMap.put("search", username.toLowerCase());
                        hashMap.put("high_light","no");

                        reference.setValue(hashMap).addOnCompleteListener(task1 -> {
                            if(task1.isSuccessful()){
                                Intent intent = new Intent(MainActivity.this, Chat_Main.class);
                                startActivity(intent);
                            }
                        });
                    }else{
                        Toast.makeText(MainActivity.this, "This Username & Password is alread exist !!!", Toast.LENGTH_SHORT).show();

                    }
                });
    }


}